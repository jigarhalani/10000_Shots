<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Candidate extends Authenticatable
{
    protected $table='candidate';
    protected $primaryKey='candidate_id';
    protected $fillable = [
        'email_id','password','candidate_type'
    ];

    public function team()
    {
        return $this->hasOne(Team::class, 'team_id');
    }
    public function candidateInfo(){
        return$this->hasOne(CandidateInfo::class,'candidate_id');
    }
    public function Score(){
        return $this->hasMany(ScoreBoard::class,'candidate_id');
    }

    // in candidate table we have player , coach & parents so we need all relation

    //relation for player to find parents.
    public function parents(){
        return $this->belongsToMany('App\Candidate', 'candidate_parents','candidate_id','parents_id');
    }

    //relation for player to find coach.
    public function coach(){
        return $this->belongsToMany('App\Candidate', 'candidate_coach','candidate_id','coach_id');
    }

    //relation for parents to find child.
    public function child(){
        return $this->belongsToMany('App\Candidate', 'candidate_parents','parents_id');
    }

    //relation for coach to find student.
    public function students(){
        return $this->belongsToMany('App\Candidate','candidate_coach','coach_id');
    }

}
