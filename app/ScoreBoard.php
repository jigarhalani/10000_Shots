<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScoreBoard extends Model
{
    protected $table='scoreboard';
    protected $primaryKey='score_id';

    public function shot(){
        return $this->hasOne(Shots::class, 'shot_id');
    }

    public function team(){
        return $this->hasOne(Team::class, 'team_id');
    }
    public function candidate(){
        return $this->hasOne(Candidate::class, 'candidate_id','candidate_id');
    }
}
