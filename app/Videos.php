<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Videos extends Model
{
    protected $table = 'videos';
    protected $primaryKey = 'video_id';
    public $timestamps = true;
    protected $guarded = [];


    public  function shot(){

        return $this->hasOne('App\Shots','shot_id','shot_id');
    }
}
