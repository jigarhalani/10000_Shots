<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CandidateInfo extends Model
{
    protected $table='candidate_info';
    protected $primaryKey='candidate_info_id';
}
