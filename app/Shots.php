<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shots extends Model
{

    protected $table = 'shots';
    protected $primaryKey = 'shot_id';
    public $timestamps = true;
    protected $guarded = [];


    public  function video(){

        return $this->hasMany('App\Videos','shot_id','shot_id');
    }

    public  function trick(){

        return $this->hasMany('App\Tricks','shot_id','shot_id');
    }
}
