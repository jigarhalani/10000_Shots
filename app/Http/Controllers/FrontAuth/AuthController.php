<?php
namespace App\Http\Controllers\FrontAuth;

use App\Candidate;
use App\CandidateParents;
use App\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;

class AuthController extends Controller
{

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectTo = '/shots';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:candidates',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return Candidate::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function adminLogin()
    {
        return view('frontpages/screen/login');
    }

    public function redirectLogin(Request $request){
        $data = $request->all();
        if(!empty($data)){
        $id = (isset($data['coach'])) ? '1' : '0';
        return view('frontpages/screen/loginscreen',['id'=>$id]);
        }else{
            return Redirect::to('login');
        }
    }

    public function adminRegister(){

        return view('frontpages/screen/welcome');
    }
    public function adminRegisterPost(Request $request)
    {
        $data = $request->all();
        $existing_user = Candidate::where('email_id',$data['email_id'])->get();
        $existing_parents_user = Candidate::where('email_id',$data['parents_email'])->get();
        if(!empty($existing_user['0'])){
            return Redirect::to('register')->with('error','Email used for player or parents is already exist in our system, please use different one.');
        }
        if(!empty($existing_parents_user['0'])){
            return Redirect::to('register')->with('error','Email used for player or parents is already exist in our system, please use different one.');
        }
        $candidate = Candidate::create([
            'email_id' => $data['email_id'],
            'password' => bcrypt($data['password']),
            'candidate_type'=>'2',
        ]);

        if(!empty($data['parents_email'])){
            $pw = str_random(10);
            $candidateParents = Candidate::create([
                'email_id'=> $data['parents_email'],
                'candidate_type'=>'3',
                'password'=> bcrypt($pw)
            ]);

            CandidateParents::create([
                'candidate_id'=> $candidate->candidate_id,
                'parents_id'=> $candidateParents->candidate_id,
            ]);
            $requestData = ['password'=>$pw,'username'=>$data['parents_email']];
            Mail::send('frontpages.screen.send_parent_email', $requestData, function($message) use ($requestData) {
                $message->subject('Welcome to 10000 Shots');
                $message->to($requestData['username']);
                $message->from('dhaval.prajapati333@gmail.com');
            });
        }
        return Redirect::to('login')->with('success','User created succesfully');
    }

    public function getDashboard(){
            return view('frontpages.screen.dashboard');
    }

    public function adminLoginPost(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        if (auth()->guard('candidate')->attempt(['email_id' => $request->input('email'), 'password' => $request->input('password')]))
        {
            $user = auth()->guard('candidate')->user();
            return Redirect::to('front/shots');
        }else{
            Session::flash('error','your username and password are wrong.');
            Session::flash('error_class','alert-danger');
            return Redirect::to('login');

        }
    }
}