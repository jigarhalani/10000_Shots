<?php

namespace App\Http\Controllers\Front;

use App\Candidate;
use App\Http\Requests;
use App\User;
use App\Videos;
use Illuminate\Http\Request;
use App\Shots;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontpages.screen.loadingscreen1');
    }


    public function viewshots(){
        $shots=Shots::where('is_active','1')->orderBy('shot_name', 'desc')->get();
        return view('frontpages.screen.listshots',['shots'=>$shots]);
    }

    public function shots($id){
        $shots = Shots::findOrFail($id);
        $video=Videos::where('is_active','1')->where('shot_id',$id)->with('shot')->get();
        return view('frontpages.screen.viewshots', ['shots'=>$shots,'videos'=>$video]);
    }

    public function showvideo($id){
        $video=Videos::where('is_active','1')->where('shot_id',$id)->with('shot')->get();
        $shots = Shots::findOrFail($id);
        return view('frontpages.screen.shotvideos', ['videos'=>$video,'shot'=>$shots]);
    }

    public function videos(){
        $videos=Videos::where('is_active',1)->with('shot')->get();
        return view('frontpages.screen.videos',['videos'=>$videos]);
    }

}
