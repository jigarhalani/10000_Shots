<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response;
use View;
use App\Shots;
use Session;
use Redirect;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
class ShotsController extends Controller
{
    //

    public function index()
    {
        return View::make('adminpages/shots/index');
    }

    public function save(Request $request)
    {
        $data=$request->input();
        $data['is_active']='1';
        $rules = array(
            'shot_name' => 'required|min:2',
        );
        $validator = Validator::make($data,$rules);
        if ($validator->fails()){
            Session::flash('message',[
                'msg'=>"Something went wrong!!",
                'type'=>'alert-danger',
            ]);
        }
        else{
            Shots::create($data);
            Session::flash('message',[
                'msg' => 'Added successfully.Thank you!!',
                'type' =>"alert-success"
            ]);
        }
        return Redirect::to('admin/shots/add');
    }


    public function view()
    {
        $shots=Shots::where('is_active','1')->orderBy('shot_name', 'desc')->paginate(PAGINATION);
        if($shots->count()==0 && $shots->currentPage()>1){
            return Redirect::to($shots->previousPageUrl());
        }
        return View::make('adminpages/shots/view',['shots'=>$shots]);
    }

    public function delete($id){

        $shots=Shots::with("trick")->find($id);
        $shots->is_active=0;
        $shots->trick()->update(['is_active' => 0]);
        $shots->video()->update(['is_active' => 0]);
        $shots->save();
        Session::flash('message',[
            'msg' => 'Deleted Successfully.Thank you!!',
            'type' =>"alert-success"
        ]);
        return Redirect::back();

    }

    public function edit($id)
    {
        $shots = Shots::findOrFail($id);
        return view('adminpages/shots.edit', ['shots'=>$shots]);
    }

    public function update($id, Request $request)
    {
        $rules = array(
            'shot_name' => 'required|min:2',
        );
        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            Session::flash('message',[
                'msg'=>"Something went wrong!!",
                'type'=>'alert-danger',
            ]);
        }
        else{
            $shots = Shots::findOrFail($id);
            $shots->update($request->all());
            Session::flash('message',[
                'msg' => 'Updated successfully.Thank you!!',
                'type' =>"alert-success"
            ]);
        }
        return Redirect::back();
    }
}
