<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
class EmailManagementController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($user_type=4,$status=3)
    {
        $keyword = Input::get('search');

        $email = DB::table('candidate')->select('*');
        if($user_type!=4)
            $email->where('candidate_type',$user_type);
        if($status!=3)
            $email->where('is_active',$status);
        if (!empty($keyword)) {
            $email->where('email_id', 'like', '%' . $keyword . '%');
        }
        $email=$email->paginate(10);
        return view('adminpages.email.index',['emails'=>$email,'user_type'=>$user_type,'status'=>$status]);
    }

    public function export($user_type,$status){

        $filename = "emails.csv";
        $handle = fopen($filename, 'w+');
        fputcsv($handle, array('Email Id', 'User Name', 'Status','User Type'));
        $email = DB::table('candidate')->select('*');
            if($user_type!=4)
                $email->where('candidate_type',$user_type);

            if($status!=3)
                $email->where('is_active',$status);
        $emails=$email->get();
        foreach($emails as $email) {
            $is_active=($email->is_active)?'Active':'Not Active';
            if($email->candidate_type=='1'){
                $candidate_type_string='Coach';
            }
            elseif ($email->candidate_type=='2'){
                $candidate_type_string='Player';
            }
            else{
                $candidate_type_string='Parents';
            }
            fputcsv($handle, array($email->email_id,$email->first_name.' '.$email->last_name,$is_active,$candidate_type_string));
        }
        fclose($handle);
        $headers = array(
            'Content-Type' => 'text/csv',
        );
        return Response::download($filename, 'emails.csv', $headers);
    }

}
