<?php

namespace App\Http\Controllers\Admin;

use App\Tricks;
use App\Videos;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response;
use View;
use App\Shots;
use Session;
use Redirect;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
class TricksController extends Controller
{
    //

    public function index()
    {
        $shots=Shots::where('is_active','1')->get();
        return View::make('adminpages.tricks.index',['shots'=>$shots]);
    }

    public function save(Request $request)
    {
        $data=$request->input();
        $data['is_active']='1';
        $rules = array(
            'trick_title' => 'required|min:2',
            'shot_id' => 'required',
        );
        $validator = Validator::make($data,$rules);
        if ($validator->fails()){
            Session::flash('message',[
                'msg'=>"Something went wrong!!",
                'type'=>'alert-danger',
            ]);
        }
        else{
            Tricks::create($data);
            Session::flash('message',[
                'msg' => 'Added successfully.Thank you!!',
                'type' =>"alert-success"
            ]);
        }
        return Redirect::to('admin/trick/add');
    }


    public function view()
    {
        $trick=Tricks::where('is_active','1')->with('shot')->paginate(PAGINATION);
        if($trick->count()==0 && $trick->currentPage()>1){
            return Redirect::to($trick->previousPageUrl());
        }
        return View::make('adminpages/tricks/view',['tricks'=>$trick]);
    }

    public function delete($id){

        $trick=Tricks::find($id);
        $trick->is_active=0;
        $trick->save();
        Session::flash('message',[
            'msg' => 'Deleted Successfully.Thank you!!',
            'type' =>"alert-success"
        ]);
        return Redirect::back();

    }

    public function edit($id)
    {
        $trick = Tricks::findOrFail($id);
        $shots=Shots::where('is_active','1')->get();
        return view('adminpages.tricks.edit', ['tricks'=>$trick,'shots'=>$shots]);
    }

    public function update($id, Request $request)
    {
        $rules = array(
            'trick_title' => 'required|min:2',
            'shot_id' => 'required',
        );
        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            Session::flash('message',[
                'msg'=>"Something went wrong!!",
                'type'=>'alert-danger',
            ]);
        }
        else{
            $trick = Tricks::findOrFail($id);
            $trick->update($request->all());
            Session::flash('message',[
                'msg' => 'Updated successfully.Thank you!!',
                'type' =>"alert-success"
            ]);
        }
        return Redirect::back();
    }
}
