<?php

namespace App\Http\Controllers\Admin;

use App\Videos;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response;
use View;
use App\Shots;
use Session;
use Redirect;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
class VideoController extends Controller
{
    //

    public function index()
    {
        $shots=Shots::where('is_active','1')->get();
        return View::make('adminpages.video.index',['shots'=>$shots]);
    }

    public function save(Request $request)
    {
        $data=$request->input();
        $data['is_active']='1';
        $rules = array(
            'video_title' => 'required|min:2',
            'video_link' => 'required||url',
            'shot_id' => 'required',
        );
        $validator = Validator::make($data,$rules);
        if ($validator->fails()){
            Session::flash('message',[
                'msg'=>"Something went wrong!!",
                'type'=>'alert-danger',
            ]);
        }
        else{
            Videos::create($data);
            Session::flash('message',[
                'msg' => 'Added successfully.Thank you!!',
                'type' =>"alert-success"
            ]);
        }
        return Redirect::to('admin/video/add');
    }


    public function view()
    {
        $video=Videos::where('is_active','1')->with('shot')->paginate(PAGINATION);
        if($video->count()==0 && $video->currentPage()>1){
            return Redirect::to($video->previousPageUrl());
        }
        return View::make('adminpages/video/view',['videos'=>$video]);
    }

    public function delete($id){

        $video=Videos::find($id);
        $video->is_active=0;
        $video->save();
        Session::flash('message',[
            'msg' => 'Deleted Successfully.Thank you!!',
            'type' =>"alert-success"
        ]);
        return Redirect::back();

    }

    public function edit($id)
    {
        $video = Videos::findOrFail($id);
        $shots=Shots::where('is_active','1')->get();
        return view('adminpages.video.edit', ['videos'=>$video,'shots'=>$shots]);
    }

    public function update($id, Request $request)
    {
        $rules = array(
            'video_title' => 'required|min:2',
            'video_link' => 'required|url',
            'shot_id' => 'required',
        );
        $validator = Validator::make($request->input(),$rules);
        if ($validator->fails()){
            Session::flash('message',[
                'msg'=>"Something went wrong!!",
                'type'=>'alert-danger',
            ]);
        }
        else{
            $video = Videos::findOrFail($id);
            $video->update($request->all());
            Session::flash('message',[
                'msg' => 'Updated successfully.Thank you!!',
                'type' =>"alert-success"
            ]);
        }
        return Redirect::back();
    }
}
