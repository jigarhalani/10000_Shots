<?php

namespace App\Http\Controllers\Admin;

use App\Candidate;
use App\CandidateInfo;
use App\ScoreBoard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\View;

class CandidateController extends Controller
{
    public function index($id){
        $data = Candidate::where('candidate_type','=',$id)->with('team')->get();
        $user_type_label=(count($data)>0)?$data[0]->candidate_type:'';
        return view('adminpages.candidate.candidate', compact('data','user_type_label'));
    }
    public function detail($id){
        $data = Candidate::where('candidate.candidate_id','=',$id)->with(['team','candidateInfo'])->get();

        return view('adminpages.candidate.candidate_detail', compact('data'));
    }
    public function shots($id){
        $data = ScoreBoard::where('candidate_id','=',$id)->with(['shot','candidate'])->get();
        return view('adminpages.candidate.scoreboard', compact('data'));
    }

    public function view_parents($id){
        $data = Candidate::where('candidate_id','=',$id)->with(['parents'])->first();
        $data=$data->parents;
        $user_type_label=(count($data)>0)?$data[0]->candidate_type:'';
        return view('adminpages.candidate.candidate', compact('data','user_type_label'));
    }

    public function view_child($id){
        $data = Candidate::where('candidate_id','=',$id)->with(['child'])->first();
        $data=$data->child;
        $user_type_label=(count($data)>0)?$data[0]->candidate_type:'';
        return view('adminpages.candidate.candidate', compact('data','user_type_label'));
    }

    public function view_coach($id){
        $data = Candidate::where('candidate_id','=',$id)->with(['coach'])->first();
        $data=$data->coach;
        $user_type_label=(count($data)>0)?$data[0]->candidate_type:'';
        return view('adminpages.candidate.candidate', compact('data','user_type_label'));
    }

    public function view_players($id){
        $data = Candidate::where('candidate_id','=',$id)->with(['students'])->first();
        $data=$data->students;
        $user_type_label=(count($data)>0)?$data[0]->candidate_type:'';
        return view('adminpages.candidate.candidate', compact('data','user_type_label'));
    }

}
