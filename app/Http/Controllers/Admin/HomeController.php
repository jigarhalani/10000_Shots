<?php

namespace App\Http\Controllers\Admin;

use App\Candidate;
use App\Http\Requests;
use App\User;
use Illuminate\Http\Request;
use App\Shots;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function checkMail(Request $request){
        $email = $request->get('email');
        if (User::where('email', '=', $email)->exists()) {
            return 'false';
        }else{
            return 'true';
        }
    }
    public function checkUsername(Request $request){
        $username = $request->get('username');
        if (User::where('username', '=', $username)->exists()) {
            return 'false';
        }else{
            return 'true';
        }
    }

    public function dashboard(){
        $total['coach_total']=Candidate::where(['candidate_type'=>1,'is_active'=>1])->count();
        $total['player_total']=Candidate::where(['candidate_type'=>2,'is_active'=>1])->count();
        $total['parents_total']=Candidate::where(['candidate_type'=>3,'is_active'=>1])->count();
        $total['shots_total']=Shots::where('is_active',1)->count();
        return view('adminpages.home.dashboard',['total'=>$total]);
    }

    public function request_registration(){
        return view('adminpages.request.index');
    }
    public function save_request_registration(Request $request)
    {
        $requestData=$request->input();
        $rules = array(
            'email_id' => 'required|min:2|unique:candidate',
        );
        $validator = Validator::make($requestData,$rules);
        if ($validator->fails()){
            Session::flash('message',[
                'msg'=>"Email Already Exist",
                'type'=>'alert-danger',
            ]);
        }
        else{
            Mail::send('adminpages.request.email_template', $requestData, function($message) use ($requestData) {
                $message->subject('Registration Link');
                $message->to($requestData['email_id']);
                $message->from('dhaval.prajapati333@gmail.com');
            });

            Session::flash('message',[
                'msg' => 'Registration link send successfully.Thank you!!',
                'type' =>"alert-success"
            ]);
        }

        return Redirect::to('admin/request_registration');
    }
}
