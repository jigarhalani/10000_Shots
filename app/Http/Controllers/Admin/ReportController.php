<?php

namespace App\Http\Controllers\Admin;

use App\Candidate;
use App\countries;
use App\ScoreBoard;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;

class ReportController extends Controller
{
    public function exportShotsByUser(Request $requests){
        $filter = $requests->all();
        if($filter['score']=='DESC'){
            $data = Candidate::whereCandidateType(2)->with('Score','candidateInfo')->get()->sortByDesc(function($data)
            {
                return $data->score->count();
            });
        }else{
            $data = Candidate::whereCandidateType(2)->with('Score','candidateInfo')->get()->sortBy(function($data)
            {
                return $data->score->count();
            });
        }

        if($filter['country'] != 0){
            $data = $data->filter(function ($value, $key) use ($filter)  {
                return $value->candidateInfo['country'] == $filter['country'];
            });
        }
        if($filter['age'] != 0){
            if($filter['age']==1){
                $data = $data->filter(function ($value, $key) use ($filter)  {
                    if(!empty($value->candidateInfo['dob'])){
                        return Carbon::parse($value->candidateInfo['dob'])->diffInYears(Carbon::now()) > 10 && Carbon::parse($value->candidateInfo['dob'])->diffInYears(Carbon::now()) < 18;
                    }

                });
            }elseif($filter['age']==2){
                $data = $data->filter(function ($value, $key) use ($filter)  {
                    if(!empty($value->candidateInfo['dob'])){
                        return Carbon::parse($value->candidateInfo['dob'])->diffInYears(Carbon::now()) > 18 && Carbon::parse($value->candidateInfo['dob'])->diffInYears(Carbon::now()) < 25;
                    }

                });
            }else{
                $data = $data->filter(function ($value, $key) use ($filter)  {
                    if(!empty($value->candidateInfo['dob'])){
                        return Carbon::parse($value->candidateInfo['dob'])->diffInYears(Carbon::now()) > 25;
                    }

                });
            }

        }
        $filename = "score_by_user.csv";
        $handle = fopen($filename, 'w+');
        if(count($data)==0){
            return Redirect::back()->with('no_record','No Records Found!');
        }
        fputcsv($handle, array('Email', 'Name','Score'));
        foreach($data as $value){
            fputcsv($handle, array($value->email_id,$value->first_name.' '.$value->last_name,count($value->Score)*100));
        }
        fclose($handle);
        $headers = array(
            'Content-Type' => 'text/csv',
        );
        return Response::download($filename, 'score_by_user.csv', $headers);
    }
    public function exportTimeWiseUser($data,$file)
    {
        $filename = $file;
        $handle = fopen($filename, 'w+');
        fputcsv($handle, array('Name', 'Email', 'Phone', 'Type', 'Status'));
        foreach ($data as $value) {
            if ($value->candidate_type == 1) {
                $type = 'Coach';
            } elseif ($value->candidate_type = 2) {
                $type = 'Player';
            } else {
                $type = "Parent";
            }
            $status = ($value->is_active == 0) ? 'Active' : 'Not Active';
            fputcsv($handle, array($value->first_name . ' ' . $value->last_name, $value->email_id, $value->phone, $type, $status));
        }
        fclose($handle);
        $headers = array(
            'Content-Type' => 'text/csv',
        );
        return Response::download($filename, $file, $headers);
    }
    public function byYear(){
        $now = Carbon::now();
        $year = $now->year;
        $data = Candidate::whereYear('created_at','=',$year)->get();
        if(count($data)==0){
            return Redirect::back()->with('no_record','No Records Found!');
        }
        $filename = 'users_added_this_year.csv';
        return $this->exportTimeWiseUser($data,$filename);
    }

    public function byMonth(){
        $now = Carbon::now();
        $month = $now->month;
        $data = Candidate::whereMonth('created_at','=',$month)->get();
        if(count($data)==0){
            return Redirect::back()->with('no_record','No Records Found!');
        }
        $filename = 'users_added_this_month.csv';
        return $this->exportTimeWiseUser($data,$filename);
    }

    public function byWeek(){
        $yesterday = Carbon::now()->subDays(1);
        $one_week_ago = Carbon::now()->subWeeks(1);
        $data = Candidate::where('created_at', '>=', $one_week_ago)->where('created_at', '<=', $yesterday)->get();
        if(count($data)==0){
            return Redirect::back()->with('no_record','No Records Found!');
        }
        $filename = 'users_added_last_week.csv';
        return $this->exportTimeWiseUser($data,$filename);
    }

    public function byToday(){
        $data = Candidate::where('created_at','>=',Carbon::today()->toDateString())->get();
        if(count($data)==0){
            return Redirect::back()->with('no_record','No Records Found!');
        }
        $filename = 'users_added_today.csv';
        return $this->exportTimeWiseUser($data,$filename);
    }

    public function index(){
        $data = countries::all();
        $country = $data->pluck('name','id')->toArray();
        $null_data = ['0'=>'All'];
        $country_data = $null_data + $country;
        return view('adminpages.admin.report')->with('countries',$country_data);
    }
}
