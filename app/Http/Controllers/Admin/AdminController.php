<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Session;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function __construct()
    {
        $this->middleware('admin_access');
    }
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $admin = User::where('first_name', 'like', '%' . $keyword . '%')->where('is_active','1')->paginate($perPage);
        } else {
            $admin = User::where('is_active','1')->paginate($perPage);
        }

        return view('adminpages.admin.index', compact('admin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('adminpages.admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();
        $password = $this->random_password(8);
        $requestData['password'] = $password;

        // for upload image

        if(isset($requestData['image'])){
            $file_name = $request->file('image');
            $ext = $file_name->getClientOriginalExtension();
            if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif'){
                $destinationPath = public_path() .'/images/admin/';
                $filename = $file_name->getClientOriginalName();
                $file_name->move($destinationPath, $filename);
                $requestData['image'] = $filename;
            }else{
                \Illuminate\Support\Facades\Session::flash('warning_message','Please Upload Image with JPG , JPEG or PNG Extension');
                return Redirect::back();
            }
        }

        // for send password in mail

        if($requestData['email']){
        Mail::send('adminpages.request.register_user_template', $requestData, function($message) use ($requestData) {
            $message->subject('Welcome to 10000 Shots');
            $message->to($requestData['email']);
            $message->from('dhaval.prajapati333@gmail.com');
        });
        }
        $requestData['password'] = Hash::make($password);
        User::create($requestData);

        Session::flash('flash_message', 'Admin added!');

        return redirect('admin/admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $admin = User::findOrFail($id);

        return view('adminpages.admin.show', compact('admin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $admin = User::findOrFail($id);

        return view('adminpages.admin.edit', compact('admin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        if(isset($requestData['image'])){
            $file_name = $request->file('image');
            $ext = $file_name->getClientOriginalExtension();
            if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif'){
                $destinationPath = public_path() .'/images/admin/';
                $filename = $file_name->getClientOriginalName();
                $file_name->move($destinationPath, $filename);
                $requestData['image'] = $filename;
            }else{
                \Illuminate\Support\Facades\Session::flash('warning_message','Please Upload Image with JPG , JPEG or PNG Extension');
                return Redirect::back();
            }
        }
        $admin = User::findOrFail($id);
        $admin->update($requestData);

        Session::flash('flash_message', 'Admin updated!');

        return redirect('admin/admin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $admin = User::findOrFail($id);
        $admin->is_active=0;
        $admin->save();

        Session::flash('flash_message', 'Admin deleted!');

        return redirect('admin/admin');
    }

    function random_password( $length = 8 ) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
        $password = substr( str_shuffle( $chars ), 0, $length );
        return $password;
    }

    function change_password(){
        return view('adminpages.admin.password');
    }

    function update_password(Request $request){
        $user = Auth::user();

        $validator = Validator::make($request->input(),[
            'old_password'          => 'required',
            'password'              => 'required|min:4|confirmed',
            'password_confirmation' => 'required'
        ]);
        if(!Hash::check($request->input('old_password'), $user->password)){
            Session::flash('message',[
                'msg'=>"The specified password does not match the database password",
                'type'=>'alert-danger',
            ]);
        }
        else if(!$validator->fails()){
            $user->password = Hash::make($request->input('password'));
            $user->save();
            Session::flash('message',[
                'msg' => 'Updated successfully.Thank you!!',
                'type' =>"alert-success"
            ]);
        }
        return redirect()->back()->withErrors($validator->errors());
    }



}
