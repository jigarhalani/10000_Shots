<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::get('/charts', function()
{
    return View::make('mcharts');
});

Route::get('/tables', function()
{
    return View::make('table');
});

Route::get('/forms', function()
{
    return View::make('form');
});

Route::get('/grid', function()
{
    return View::make('grid');
});

Route::get('/buttons', function()
{
    return View::make('buttons');
});


Route::get('/icons', function()
{
    return View::make('icons');
});

Route::get('/panels', function()
{
    return View::make('panel');
});

Route::get('/typography', function()
{
    return View::make('typography');
});

Route::get('/notifications', function()
{
    return View::make('notifications');
});

Route::get('/blank', function()
{
    return View::make('blank');
});

/*Route::get('/login', function()
{
    return View::make('login');
});*/

Route::get('/documentation', function()
{
    return View::make('documentation');
});



    Route::group(['prefix'=>'admin/'],function() {
        Route::group(['middleware' => ['auth']], function () {
            Route::group(['prefix' => 'shots/'], function () {
                Route::get('add', 'Admin\ShotsController@index');
                Route::post('save', 'Admin\ShotsController@save');
                Route::get('view', 'Admin\ShotsController@view');
                Route::get('edit/{id}', 'Admin\ShotsController@edit');
                Route::get('delete/{id}', 'Admin\ShotsController@delete');
                Route::post('update/{id}', 'Admin\ShotsController@update');
            });

            Route::group(['prefix' => 'video/'], function () {
                Route::get('add', 'Admin\VideoController@index');
                Route::post('save', 'Admin\VideoController@save');
                Route::get('view', 'Admin\VideoController@view');
                Route::get('edit/{id}', 'Admin\VideoController@edit');
                Route::get('delete/{id}', 'Admin\VideoController@delete');
                Route::post('update/{id}', 'Admin\VideoController@update');
            });

            Route::group(['prefix' => 'trick/'], function () {
                Route::get('add', 'Admin\TricksController@index');
                Route::post('save', 'Admin\TricksController@save');
                Route::get('view', 'Admin\TricksController@view');
                Route::get('edit/{id}', 'Admin\TricksController@edit');
                Route::get('delete/{id}', 'Admin\TricksController@delete');
                Route::post('update/{id}', 'Admin\TricksController@update');
            });

            Route::group(['prefix' => 'email/'], function () {
                Route::get('/{user_type?}/{status?}', 'Admin\EmailManagementController@index');
                Route::get('/export/{user_type}/{status}', 'Admin\EmailManagementController@export');
            });
            Route::get('/', 'Admin\HomeController@dashboard');
            Route::get('/home', 'Admin\HomeController@dashboard');
            Route::get('/dashboard', 'Admin\HomeController@dashboard');
            Route::get('/users/{id}', 'Admin\CandidateController@index');
            Route::get('/users/detail/{id}', 'Admin\CandidateController@detail');
            Route::get('/users/shots/{id}','Admin\CandidateController@shots');
            Route::get('/users/view_parents/{id}','Admin\CandidateController@view_parents');
            Route::get('/users/view_child/{id}','Admin\CandidateController@view_child');
            Route::get('/users/view_coach/{id}','Admin\CandidateController@view_coach');
            Route::get('/users/view_players/{id}','Admin\CandidateController@view_players');
            Route::post('/checkmail', 'Admin\HomeController@checkMail');
            Route::post('/checkusername', 'Admin\HomeController@checkUsername');
            Route::get('/request_registration', 'Admin\HomeController@request_registration');
            Route::post('/save_request_registration', 'Admin\HomeController@save_request_registration');
            Route::resource('/admin', 'Admin\\AdminController');
            Route::get('/change_password','Admin\AdminController@change_password');
            Route::post('/change_password','Admin\AdminController@update_password');
            Route::get('/home', 'Admin\HomeController@dashboard');
            Route::post('export-shots-by-user','Admin\ReportController@exportShotsByUser');
            Route::get('users-this-year','Admin\ReportController@byYear');
            Route::get('users-this-month','Admin\ReportController@byMonth');
            Route::get('users-this-week','Admin\ReportController@byWeek');
            Route::get('users-added-today','Admin\ReportController@byToday');
            Route::get('/report','Admin\ReportController@index');
        });
            Route::auth();
        });
Route::group(['middleware' => 'front_access'], function () {
    Route::get('dashboard', 'FrontAuth\AuthController@getDashboard');
});
Route::get('login', 'FrontAuth\AuthController@adminLogin');
Route::post('login', ['as'=>'front-login','uses'=>'FrontAuth\AuthController@adminLoginPost']);
Route::post('checkUser', ['as'=>'front-login','uses'=>'FrontAuth\AuthController@redirectLogin']);
Route::get('checkUser', ['as'=>'front-login','uses'=>'FrontAuth\AuthController@redirectLogin']);
Route::get('register', 'FrontAuth\AuthController@adminRegister');
Route::post('register', ['as'=>'front-login','uses'=>'FrontAuth\AuthController@adminRegisterPost']);

    Route::group(['prefix'=>'front'],function (){
        Route::get('/home', 'Front\HomeController@index');

        Route::group(['prefix'=>'shots'],function () {
            Route::get('/', 'Front\HomeController@viewshots');
            Route::get('/{id}', 'Front\HomeController@shots');
            Route::get('/video/{id}', 'Front\HomeController@showvideo');
        });

        Route::get('videos','Front\HomeController@videos');

    });


    Route::get('/', function (){
            if(\Illuminate\Support\Facades\Auth::check()){
                return \Illuminate\Support\Facades\Redirect::to('admin');
            }else {
                return \Illuminate\Support\Facades\Redirect::to('admin/login');
            }
});