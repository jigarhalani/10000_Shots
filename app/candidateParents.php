<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CandidateParents extends Model
{
    protected $table='candidate_parents';
    protected $fillable = [
        'candidate_id','parents_id'
    ];

}
