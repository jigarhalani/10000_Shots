<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\App;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        require app_path() . '/constants.php';
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
            //symlink(storage_path('app/public/uploads'),public_path('storage/uploads'));
            //App::make('files')->copyDirectory(storage_path('app\public\uploads'), public_path('storage\uploads'));
            $this->app->register('Appzcoder\CrudGenerator\CrudGeneratorServiceProvider');

    }
}
