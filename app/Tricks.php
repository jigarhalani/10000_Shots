<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tricks extends Model
{

    protected $table = 'tricks';
    protected $primaryKey = 'trick_id';
    public $timestamps = true;
    protected $guarded = [];


    public  function shot(){

        return $this->hasOne('App\Shots','shot_id','shot_id');
    }
}
