<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TricksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tricks',function (Blueprint $table){

            $table->increments('trick_id');
            $table->string('trick_title');
            $table->text('trick_description');
            $table->integer('shot_id');
            $table->smallInteger('is_active')->default(0);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tricks');
    }
}
