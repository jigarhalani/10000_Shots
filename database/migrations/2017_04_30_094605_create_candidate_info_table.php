<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidateInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidate_info', function (Blueprint $table) {
            $table->increments('candidate_info_id');
            $table->integer('candidate_id');
            $table->text('address_line1');
            $table->text('address_line2');
            $table->date('dob');
            $table->string('organization');
            $table->string('position');
            $table->string('country');
            $table->string('state');
            $table->string('city');
            $table->string('yearsplayed');
            $table->smallInteger('is_active')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('candidate_info');
    }
}
