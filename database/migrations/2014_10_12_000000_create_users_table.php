<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('phone');
            $table->string('password');
            $table->smallInteger('is_active')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
        \Illuminate\Support\Facades\DB::table('users')->insert([
                'first_name' => 'admin',
                'email' => 'admin@admin.com',
                'password' => '$2y$10$xhS6Io1Z/tn4P.mW7p.Mmep45FB7BKUEexpslCAIpj2sMaRA2lhyO',
                'phone' => '98745698745',
                'is_active'=>'1',
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
