<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidate', function (Blueprint $table) {
            $table->increments('candidate_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email_id')->unique();
            $table->enum('candidate_type',array('1','2','3'))->default('1')->comment('1-> coach, 2->player , 3->parents');
            $table->integer('image_id')->nullable();
            $table->integer('team_id');
            $table->string('phone');
            $table->string('password');
            $table->smallInteger('is_active')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('candidate');
    }
}
