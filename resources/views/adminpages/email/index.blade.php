@extends('adminpages.layouts.dashboard')
@section('page_heading','Emails')
@section('section')
    @include('adminpages.includes.notification')
    <div class="row">
        <?php

        ?>
        <div class="col-lg-12">

            <div class="panel panel-default">

                <div class="panel-heading">

                    View Emails

                </div>

                <div class="panel-body">

                    <div class="table-responsive">
                        <div class="col-md-12">
                        <div class="form-group pull-left">
                            Select User Type
                            <select id="user_type_filter">
                                <option value="4" @if($user_type=='4') {{ 'selected' }} @endif>All</option>
                                <option value="1" @if($user_type=='1') {{ 'selected' }} @endif>Coach</option>
                                <option value="2" @if($user_type=='2') {{ 'selected' }} @endif>Player</option>
                                <option value="3" @if($user_type=='3') {{ 'selected' }} @endif>Parents</option>
                            </select>
                            Select User Status
                            <select id="user_status_filter">
                                <option value="3" @if($status=='3') {{ 'selected' }} @endif >Active & Deactive</option>
                                <option value="1" @if($status=='1') {{ 'selected' }} @endif>Active</option>
                                <option value="0" @if($status=='0') {{ 'selected' }} @endif>Deactive</option>
                            </select>
                            <a href="{{ url('admin/email/export/'.$user_type.'/'.$status) }}" class="btn btn-success btn-sm" title="Add New Admin">
                                Export Data
                            </a>
                        </div>
                        <div class="form-group pull-right">
                            {!! Form::open(['method' => 'GET', 'url' => '/admin/email', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                            <div class="input-group">
                                <input type="text" class="search form-control" name="search" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        </div>
                        <div class="pagination-wrapper">
                            {{ $emails->appends(['search' => Request::get('search')])->render() }}
                        </div>

                    <table class="table table-striped table-bordered table-hover results" id="dataTables-example">

                            <thead>

                            <tr>
                                <th>Email Id</th>
                                <th>Status</th>
                            </tr>
                            <tr class="warning no-result">
                                <td colspan="4"><i class="fa fa-warning"></i> No result</td>
                            </tr>
                            </thead>

                            <tbody>
                            @if(count($emails)>0)
                                @foreach($emails as $email)
                                    <tr class="odd gradeX" scope="row">
                                        <td>{{  $email->email_id }}</td>
                                        <td>
                                            @if($email->is_active==1)
                                                <span class="label label-success">Active</span>
                                            @else
                                                <span class="label label-danger">Not Active</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="warning">
                                    <td colspan="4"><i class="fa fa-warning"></i> No result</td>
                                </tr>
                            @endif


                            </tbody>

                        </table>
                        {{ $emails->appends(['search' => Request::get('search')])->render() }}
                    </div>

                    <!-- /.table-responsive -->

                </div>

                <!-- /.panel-body -->

            </div>

            <!-- /.panel -->

        </div>

        <!-- /.col-lg-12 -->

    </div>
@stop
