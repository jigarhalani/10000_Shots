<ul class="nav navbar-top-links navbar-right">
    {{--<li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
        </a>
        <ul class="dropdown-menu dropdown-alerts">
            <li>
                <a href="#">
                    <div>
                        <i class="fa fa-comment fa-fw"></i> New Comment
                        <span class="pull-right text-muted small">4 minutes ago</span>
                    </div>
                </a>
            </li>
            <li class="divider"></li>
            <li>
                <a href="#">
                    <div>
                        <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                        <span class="pull-right text-muted small">12 minutes ago</span>
                    </div>
                </a>
            </li>
            <li class="divider"></li>
            <li>
                <a href="#">
                    <div>
                        <i class="fa fa-envelope fa-fw"></i> Message Sent
                        <span class="pull-right text-muted small">4 minutes ago</span>
                    </div>
                </a>
            </li>
            <li class="divider"></li>
            <li>
                <a href="#">
                    <div>
                        <i class="fa fa-tasks fa-fw"></i> New Task
                        <span class="pull-right text-muted small">4 minutes ago</span>
                    </div>
                </a>
            </li>
            <li class="divider"></li>
            <li>
                <a href="#">
                    <div>
                        <i class="fa fa-upload fa-fw"></i> Server Rebooted
                        <span class="pull-right text-muted small">4 minutes ago</span>
                    </div>
                </a>
            </li>
            <li class="divider"></li>
            <li>
                <a class="text-center" href="#">
                    <strong>See All Alerts</strong>
                    <i class="fa fa-angle-right"></i>
                </a>
            </li>
        </ul>
        <!-- /.dropdown-alerts -->
    </li>--}}
    <!-- /.dropdown -->
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
        </a>
        <ul class="dropdown-menu dropdown-user">
            <li><a href="{{ url('admin/admin/').'/'.Auth::user()->id }}"><i class="fa fa-user fa-fw"></i> User Profile</a> </li>
            <li><a href="{{ url('admin/change_password/') }}"><i class="fa fa-key fa-fw"></i> Change Password</a> </li>
            <li class="divider"></li>
            <li><a href="{{ url ('admin/logout') }}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
            </li>
        </ul>
        <!-- /.dropdown-user -->
    </li>
    <!-- /.dropdown -->
</ul>
<!-- /.navbar-top-links -->

<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            {{--<li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                </div>
            </li>--}}
            <li {{ (Request::is('*dashboard') ? 'class=active' : '') }}>
                <a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>
            {{--<li {{ (Request::is('*charts') ? 'class="active"' : '') }}>
                <a href="{{ url ('charts') }}"><i class="fa fa-bar-chart-o fa-fw"></i> Charts</a>
                <!-- /.nav-second-level -->
            </li>
            <li {{ (Request::is('*tables') ? 'class="active"' : '') }}>
                <a href="{{ url ('tables') }}"><i class="fa fa-table fa-fw"></i> Tables</a>
            </li>
            <li {{ (Request::is('*forms') ? 'class="active"' : '') }}>
                <a href="{{ url ('forms') }}"><i class="fa fa-edit fa-fw"></i> Forms</a>
            </li>
            <li >
                <a href="#"><i class="fa fa-wrench fa-fw"></i> UI Elements<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li {{ (Request::is('*panels') ? 'class="active"' : '') }}>
                        <a href="{{ url ('panels') }}">Panels and Collapsibles</a>
                    </li>
                    <li {{ (Request::is('*buttons') ? 'class="active"' : '') }}>
                        <a href="{{ url ('buttons' ) }}">Buttons</a>
                    </li>
                    <li {{ (Request::is('*notifications') ? 'class="active"' : '') }}>
                        <a href="{{ url('notifications') }}">Alerts</a>
                    </li>
                    <li {{ (Request::is('*typography') ? 'class="active"' : '') }}>
                        <a href="{{ url ('typography') }}">Typography</a>
                    </li>
                    <li {{ (Request::is('*icons') ? 'class="active"' : '') }}>
                        <a href="{{ url ('icons') }}"> Icons</a>
                    </li>
                    <li {{ (Request::is('*grid') ? 'class="active"' : '') }}>
                        <a href="{{ url ('grid') }}">Grid</a>
                    </li>
                </ul>
            </li>--}}
            {{--<li>
                <a href="#"><i class="fa fa-sitemap fa-fw"></i> Multi-Level Dropdown<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="#">Second Level Item</a>
                    </li>
                    <li>
                        <a href="#">Second Level Item</a>
                    </li>
                    <li>
                        <a href="#">Third Level <span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="#">Third Level Item</a>
                            </li>
                            <li>
                                <a href="#">Third Level Item</a>
                            </li>
                            <li>
                                <a href="#">Third Level Item</a>
                            </li>
                            <li>
                                <a href="#">Third Level Item</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>--}}
            @if(isset(Auth::user()->role) && Auth::user()->role==1)
            <li>
                <a href="#"><i class="fa fa-user fa-fw"></i> Admin<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li {{ (Request::is('*admin/admin/create') ? 'class=active' : '') }}>
                        <a href="{{ url ('admin/admin/create') }}">ADD</a>
                    </li>
                    <li {{ (Request::is('*admin/admin') ? 'class=active' : '') }}>
                        <a href="{{ url ('admin/admin') }}">View</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            @endif
            <li>
                <a href="#"><i class="fa fa-table fa-fw"></i> Shots<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li {{ (Request::is('*shots/add') ? 'class=active' : '') }}>
                        <a href="{{ url ('admin/shots/add') }}">ADD</a>
                    </li>
                    <li {{ (Request::is('*shots/view') ? 'class=active' : '') }}>
                        <a href="{{ url ('admin/shots/view') }}">View</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="#"><i class="fa fa-video-camera fa-fw"></i> Videos<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li {{ (Request::is('*video/add') ? 'class=active' : '') }}>
                        <a href="{{ url ('admin/video/add') }}">ADD</a>
                    </li>
                    <li {{ (Request::is('*video/view') ? 'class=active' : '') }}>
                        <a href="{{ url ('admin/video/view') }}">View</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="#"><i class="fa fa-table fa-fw"></i> Tricks & Tips<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li {{ (Request::is('*trick/add') ? 'class=active' : '') }}>
                        <a href="{{ url ('admin/trick/add') }}">ADD</a>
                    </li>
                    <li {{ (Request::is('*trick/view') ? 'class=active' : '') }}>
                        <a href="{{ url ('admin/trick/view') }}">View</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li {{ (Request::is('*email*') ? 'class=active' : '') }}>
                <a href="{{ url ('admin/email') }}"><i class="fa fa-file-word-o fa-fw"></i>EMail Management</a>
            </li>
            <li>
                <a href=""><i class="fa fa-users fa-fw"></i> Users<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li {{ (Request::is('*users/1') ? 'class=active' : '') }}>
                        <a href="{{ url ('admin/users/1') }}">Coach</a>
                    </li>
                    <li {{ (Request::is('*users/2') ? 'class=active' : '') }}>
                        <a href="{{ url ('admin/users/2') }}">Players</a>
                    </li>
                    <li {{ (Request::is('*users/3') ? 'class=active' : '') }}>
                        <a href="{{ url ('admin/users/3') }}">Parents</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li {{ (Request::is('*request_registration*') ? 'class=active' : '') }}>
                <a href="{{ url ('admin/request_registration') }}"><i class="fa fa-users fa-fw"></i> Request Registration</a>
            </li>
            <li {{ (Request::is('*report') ? 'class=active' : '') }}>
                <a href="{{ url('admin/report') }}"><i class="fa fa-dashboard fa-fw"></i> Reports</a>
            </li>
            {{--<li {{ (Request::is('*documentation') ? 'class="active"' : '') }}>
                <a href="{{ url ('documentation') }}"><i class="fa fa-file-word-o fa-fw"></i> Documentation</a>
            </li>--}}
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->