@extends('adminpages.layouts.dashboard')
@section('page_heading','Register User')
@section('section')
    @include('adminpages.includes.notification')
    <div class="row">

        <div class="col-lg-12">

            <div class="panel panel-default">

                <div class="panel-heading">

                    Add Request To Register User

                </div>

                <div class="panel-body">

                    <div class="row">

                        <div class="col-lg-12">



                            <form role="form" id="request_registration_form" method="POST" action="{{ url('admin/save_request_registration') }}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label>Enter User Email Address</label>
                                    <input class="form-control" placeholder="Enter User Email Address" id="email" name="email_id" required minlength="5">
                                </div>

                                <div class="form-group">
                                        <label>User Type</label>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="user_type" id="player" value="2" checked> Player
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="user_type" id="coach" value="1"> Coach
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="user_type" id="parents" value="3"> Parents
                                            </label>
                                        </div>

                                </div>
                                <button type="submit" class="btn btn-default">Add</button>
                                <button type="reset" class="btn btn-default">Reset</button>

                            </form>

                        </div>

                    </div>

                    <!-- /.row (nested) -->

                </div>

                <!-- /.panel-body -->

            </div>

            <!-- /.panel -->

        </div>

        <!-- /.col-lg-12 -->

    </div>
@stop
