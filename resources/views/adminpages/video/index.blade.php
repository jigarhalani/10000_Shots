@extends('adminpages.layouts.dashboard')
@section('page_heading','Videos')
@section('section')
    @include('adminpages.includes.notification')
    <div class="row">

        <div class="col-lg-12">

            <div class="panel panel-default">

                <div class="panel-heading">

                    Add Video

                </div>

                <div class="panel-body">

                    <div class="row">

                        <div class="col-lg-12">



                            <form role="form" id="video_form" method="POST" action="{{ url('admin/video/save') }}">
                                {{ csrf_field() }}
                                <div class="alert alert-info">
                                    Only Youtube video link are supported.
                                    Ex: https://www.youtube.com/watch?v=Y2VF8tmLFHw
                                </div>
                                <div class="form-group">
                                    <label>Enter Video Title</label>
                                    <input class="form-control" placeholder="Enter Video Title" id="video_title" name="video_title" required minlength="2">
                                </div>

                                <div class="form-group">
                                    <label>Select Shots</label>
                                    <select id="shot_id" name="shot_id" class="form-control" required>
                                        @foreach($shots as $shot)
                                            <option value="{{$shot->shot_id}}">{{ $shot->shot_name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Enter Video Link</label>
                                    <textarea class="form-control" placeholder="https://www.youtube.com/watch?v=Y2VF8tmLFHw" id="video_link" name="video_link" required></textarea>
                                </div>

                                <button type="submit" class="btn btn-default">Add</button>
                                <button type="reset" class="btn btn-default">Reset</button>

                            </form>

                        </div>

                    </div>

                    <!-- /.row (nested) -->

                </div>

                <!-- /.panel-body -->

            </div>

            <!-- /.panel -->

        </div>

        <!-- /.col-lg-12 -->

    </div>
@stop
