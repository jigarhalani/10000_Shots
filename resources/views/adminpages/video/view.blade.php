@extends('adminpages.layouts.dashboard')
@section('page_heading','Videos')
@section('section')
    @include('adminpages.includes.notification')
    <div class="row">

        <div class="col-lg-12">

            <div class="panel panel-default">

                <div class="panel-heading">

                    View Videos

                </div>

                <div class="panel-body">

                    <div class="table-responsive">

                        {{ $videos->links() }}
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">

                            <thead>

                            <tr>
                                <th>Video Title</th>
                                <th>Video Link</th>
                                <th>Shots Name</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>

                            </thead>

                            <tbody>

                            @if(count($videos)>0)
                            @foreach($videos as $video)
                            <tr class="odd gradeX">
                                <td>{{  $video->video_title }}</td>

                                <td>
                                    <?php
                                        preg_match(
                                            '/[\\?\\&]v=([^\\?\\&]+)/',
                                            $video->video_link,
                                            $matches
                                        );
                                    ?>
                                        <iframe width="560" height="315" src="{{  'https://www.youtube.com/embed/'.$matches[1] }}" frameborder="0" allowfullscreen></iframe></td>
                                <td>{{  $video->shot->shot_name }}</td>
                                <td class="center"><a href="{{ url('admin/video/edit/'.$video->video_id) }}">Edit</a></td>
                                <td class="center"><a
                                            href="{{ url('admin/video/delete/'.$video->video_id) }}" onclick="return confirm('Are you sure you want to delete?')">Delete</a>
                                </td>
                            </tr>

                            @endforeach
                            @else
                                <tr class="warning">
                                    <td colspan="5"><i class="fa fa-warning"></i> No result</td>
                                </tr>
                            @endif
                            </tbody>

                        </table>
                        {{ $videos->links() }}
                    </div>

                    <!-- /.table-responsive -->

                </div>

                <!-- /.panel-body -->

            </div>

            <!-- /.panel -->

        </div>

        <!-- /.col-lg-12 -->

    </div>
@stop
