@extends('adminpages.layouts.dashboard')
@section('page_heading','Shots')
@section('section')
    @include('adminpages.includes.notification')
    <div class="row">

        <div class="col-lg-12">

            <div class="panel panel-default">

                <div class="panel-heading">

                    View Shots
                </div>
                <div class="panel-body">

                    <div class="table-responsive">

                        {{ $shots->links() }}

                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">

                            <thead>

                            <tr>
                                <th>Shots Name</th>
                                <th>Shots Description</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>

                            </thead>

                            <tbody>
                            @if(count($shots)>0)
                            @foreach($shots as $shot)
                            <tr class="odd gradeX">
                                <td>{{  $shot->shot_name }}</td>
                                <td>{{  $shot->shot_description }}</td>
                                <td class="center"><a href="{{ url('admin/shots/edit/'.$shot->shot_id) }}">Edit</a></td>
                                <td class="center"><a
                                            href="{{ url('admin/shots/delete/'.$shot->shot_id) }}" onclick="return confirm('Are you sure you want to delete? All video and tips which belong to this shots will also delete..')">Delete</a>
                                </td>
                            </tr>

                            @endforeach
                            @else
                                <tr class="warning">
                                    <td colspan="4"><i class="fa fa-warning"></i> No result</td>
                                </tr>
                            @endif
                            </tbody>

                        </table>
                        {{ $shots->links() }}
                    </div>

                    <!-- /.table-responsive -->

                </div>

                <!-- /.panel-body -->

            </div>

            <!-- /.panel -->

        </div>

        <!-- /.col-lg-12 -->

    </div>
@stop
