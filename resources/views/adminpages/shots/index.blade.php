@extends('adminpages.layouts.dashboard')
@section('page_heading','Shots')
@section('section')
    @include('adminpages.includes.notification')
    <div class="row">

        <div class="col-lg-12">

            <div class="panel panel-default">

                <div class="panel-heading">

                    Add Shots

                </div>

                <div class="panel-body">

                    <div class="row">

                        <div class="col-lg-12">



                            <form role="form" id="shots_form" method="POST" action="{{ url('admin/shots/save') }}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label>Enter Shot Name</label>
                                    <input class="form-control" placeholder="Enter Shot Name" id="shot_name" name="shot_name" required minlength="2">
                                </div>

                                <div class="form-group">
                                    <label>Enter Shot Description</label>
                                    <textarea class="form-control" placeholder="Enter Shot Description" id="shot_description" name="shot_description"></textarea>
                                </div>
                                <button type="submit" class="btn btn-default">Add</button>
                                <button type="reset" class="btn btn-default">Reset</button>

                            </form>

                        </div>

                    </div>

                    <!-- /.row (nested) -->

                </div>

                <!-- /.panel-body -->

            </div>

            <!-- /.panel -->

        </div>

        <!-- /.col-lg-12 -->

    </div>
@stop
