@extends('adminpages.layouts.dashboard')
@section('page_heading','Reports')
@section('section')
    <?php
    if(Session::has('no_record')){ ?>
    <div class="alert alert-danger">
        {{ Session::get('no_record') }}
    </div>
    <?php
    }
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Export Shots By User</div>
                <div class="panel-body">

                    {!! Form::open(['method' => 'POST', 'url' => '/admin/export-shots-by-user', 'class' => ''])  !!}
                    <div class="form-group {{ $errors->has('score') ? 'has-error' : ''}}">
                        {!! Form::label('score', 'Sort By Score', ['class' => 'col-md-4 control-label pull-left']) !!}
                        <div class="">
                            {!! Form::select('score', array('DESC' => 'High', 'ASC' => 'Low')) !!}
                            {!! $errors->first('score', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('country') ? 'has-error' : ''}}">
                        {!! Form::label('country', 'Country', ['class' => 'col-md-4 control-label pull-left']) !!}
                        <div class="">
                            {!! Form::select('country', $countries) !!}
                            {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('age') ? 'has-error' : ''}}">
                        {!! Form::label('age', 'Age Group', ['class' => 'col-md-4 control-label pull-left']) !!}
                        <div class="">
                            {!! Form::select('age', array('0'=>'Please select','1' => 'Age > 10 to Age < 18', '2' => 'Age > 18 to Age < 25', '3' => 'Age > 25')) !!}
                            {!! $errors->first('age', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="">
                            {!! Form::submit('Export Shots By User', ['class' => 'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>



    <a href="{{ url('admin/users-this-year') }}" data-toggle="tooltip" data-placement="bottom" title="Hooray!" type="button" class="btn btn-success btn-outline     ">Users Added This Year</a>
    <a href="{{ url('admin/users-this-month') }}" type="button" class="btn btn-success btn-outline     ">Users Added This Month</a>
    <a href="{{ url('admin/users-this-week') }}" type="button" class="btn btn-success btn-outline     ">Users Added Last Week</a>
    <a href="{{ url('admin/users-added-today') }}" type="button" class="btn btn-success btn-outline     ">Users Added Today</a>

@stop
