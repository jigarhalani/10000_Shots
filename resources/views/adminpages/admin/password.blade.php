@extends('adminpages.layouts.dashboard')
@section('page_heading','Change Password')
@section('section')
    @include('adminpages.includes.notification')
    <div class="row">

        <div class="col-lg-12">

            <div class="panel panel-default">

                <div class="panel-heading">

                    Change Password

                </div>

                <div class="panel-body">

                    <form class="form-horizontal" id='change_password' role="form" method="POST" action="{{ url('admin/change_password') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Old Password</label>

                            <div class="col-md-6">
                                <input id="oldpw" type="password" class="form-control" name="old_password" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label">New Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                            <div class="col-md-6">
                                <input id="password_confirmation" type="password" class="form-control" name="password_confirmation">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-refresh"></i> Change Password
                                </button>
                            </div>
                        </div>
                    </form>
                    <!-- /.row (nested) -->

                </div>

                <!-- /.panel-body -->

            </div>

            <!-- /.panel -->

        </div>

        <!-- /.col-lg-12 -->

    </div>
@stop
