@extends('adminpages.layouts.dashboard')
@section('page_heading','Admin')
@section('section')
        <?php
        if(Session::has('flash_message')){ ?>
        <div class="alert alert-success">
            {{ Session::get('flash_message') }}
        </div>
        <?php
        }
        if(Session::has('warning_message')){ ?>
        <div class="alert alert-danger">
            {{ Session::get('warning_message') }}
        </div>
        <?php
        }
        ?>
        <div class="row">
            {{--@include('admin.sidebar')--}}

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Admin</div>
                    <div class="panel-body">
                        <a href="{{ url('/admin/admin/create') }}" class="btn btn-success btn-sm" title="Add New Admin">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        {!! Form::open(['method' => 'GET', 'url' => '/admin/admin', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="pagination-wrapper"> {!! $admin->appends(['search' => Request::get('search')])->render() !!} </div>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>User Name</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Role</th>
                                        <th>Actions</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($admin as $item)
                                    <tr>
                                        <td>{{ $item->username }}</td>
                                        <td>{{ $item->first_name }}</td>
                                        <td>{{ $item->last_name }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>{{ $item->phone }}</td>
                                        <td>{{ ($item->role==1)?'Super Admin':'Admin' }}</td>
                                        
                                        <td>
                                            <a href="{{ url('/admin/admin/' . $item->id) }}" title="View Admin"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="{{ url('/admin/admin/' . $item->id . '/edit') }}" title="Edit Admin"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                            @if(\Illuminate\Support\Facades\Auth::user()->id!=$item->id)
                                                {!! Form::open([
                                                    'method'=>'DELETE',
                                                    'url' => ['/admin/admin', $item->id],
                                                    'style' => 'display:inline'
                                                ]) !!}
                                                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                            'type' => 'submit',
                                                            'class' => 'btn btn-danger btn-xs',
                                                            'title' => 'Delete Admin',
                                                            'onclick'=>'return confirm("Confirm delete?")'
                                                    )) !!}
                                                {!! Form::close() !!}
                                            @endif
                                        </td>
                                        <td>@if($item->is_active==1)
                                                <span class="label label-success">Active</span>
                                        @else
                                                <span class="label label-danger">Not Active</span>
                                        @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                        <div class="pagination-wrapper"> {!! $admin->appends(['search' => Request::get('search')])->render() !!} </div>

                    </div>
                </div>
            </div>
        </div>
@endsection
