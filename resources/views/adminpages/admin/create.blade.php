@extends('adminpages.layouts.dashboard')
@section('page_heading','Admin')

@section('section')
        @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif
        <?php
        if(Session::has('warning_message')){ ?>
        <div class="alert alert-danger">
            {{ Session::get('warning_message') }}
        </div>
        <?php
        }
        ?>
        <div class="row">
            {{--@include('admin.sidebar')--}}
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Create New Admin</div>
                    <div class="panel-body">
                        <a href="{{ url('/admin/admin') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        {!! Form::open(['url' => '/admin/admin', 'class' => 'form-horizontal','id'=>'add_admin', 'files' => true]) !!}

                        @include ('adminpages.admin.form')

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
@endsection
