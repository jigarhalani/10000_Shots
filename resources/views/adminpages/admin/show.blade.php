@extends('adminpages.layouts.dashboard')
@section('page_heading','Admin')

@section('section')
        <div class="row">
            {{--@include('admin.sidebar')--}}

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><strong>User Name</strong>: {{ $admin->username }}</div>
                    <div class="panel-body">

                        <a href="{{ url('/admin/admin') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/admin/' . $admin->id . '/edit') }}" title="Edit Admin"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/admin', $admin->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Admin',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <img src="{{ asset('images/admin/').'/'.$admin->image }}" class="pull-right" style="width:100px;height:100px;">
                        <br/><br/><br/><br/><br/><br/><br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>

                                <tr> <th>ID</th><td>{{ $admin->id }}</td>  </tr>
                                <tr>   <th>First Name</th><td>{{ $admin->first_name }}</td> </tr>
                                <tr>   <th>Last Name</th><td>{{ $admin->last_name }}</td> </tr>
                                <tr>   <th>Email</th><td>{{ $admin->email }}</td> </tr>
                                <tr>   <th>Phone</th><td>{{ $admin->phone }}</td> </tr>
                                <tr>   <th>Role</th><td>{{ ($admin->role=1)?'Super Admin':'Admin' }}</td> </tr>
                                <tr>   <th>Status</th><td>
                                        @if($admin->is_active==1)
                                            <span class="label label-success">Active</span>
                                        @else
                                            <span class="label label-danger">Not Active</span>
                                        @endif
                                    </td> </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
@endsection
