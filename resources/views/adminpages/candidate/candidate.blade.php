@extends('adminpages.layouts.dashboard')
<?php
if($user_type_label==1){
    $label="Coach";
}else if($user_type_label==2){
    $label="Player";
}else if($user_type_label==3){
    $label="Parents";
}
else{
    $label='';
}
$label="Users -> ".$label;
?>
@section('page_heading',$label)
@section('section')
    @if(!empty($data))
    <div class="row">
        <div class="col-sm-12">
            @section ('htable_panel_title','Users')
            @section ('htable_panel_body')
                <table id="example" class="wrapper display table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th class="no-sort">Phone</th>
                        <th class="no-sort">Status</th>
                        <th class="no-sort">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $value)
                        <tr>
                            <td>{{ $value->first_name }}</td>
                            <td>{{ $value->last_name }}</td>
                            <td>{{ $value->email_id }}</td>
                            <td>{{ $value->phone }}</td>
                            <td>
                                @if($value->is_active==1)
                                    <span class="label label-success">Active</span>
                                @else
                                    <span class="label label-danger">Not Active</span>
                                @endif
                            </td>
                            <td>
                                 <a href="{{ url('admin/users/detail/').'/'.$value->candidate_id }}" title="View">
                                 <button class="table-btn btn btn-info btn-xs"> <i class="fa fa-eye" aria-hidden="true"></i> View User</button></a>
                                @if($value->candidate_type == 2)
                                    <a href="{{ url('admin/users/view_coach/').'/'.$value->candidate_id }}" title="View">
                                    <button class="table-btn btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View Coach</button></a>
                                    <a href="{{ url('admin/users/view_parents/').'/'.$value->candidate_id }}" title="View">
                                    <button class="table-btn btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View Parents</button></a>
                                    <a href="{{ url('admin/users/shots/').'/'.$value->candidate_id }}" title="View">
                                    <button class="table-btn btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View Shots</button></a>
                                @elseif($value->candidate_type == 1)
                                    <a href="{{ url('admin/users/view_players/').'/'.$value->candidate_id }}" title="View">
                                    <button class="table-btn btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View Players</button></a>
                                @elseif($value->candidate_type == 3)
                                    <a href="{{ url('admin/users/view_child/').'/'.$value->candidate_id }}" title="View">
                                        <button class="table-btn btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View Child</button></a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{--{{ $data->links() }}--}}
            @endsection
            @include('widgets.panel', array('header'=>true, 'as'=>'htable'))
        </div>
    </div>
    @endif

@stop
