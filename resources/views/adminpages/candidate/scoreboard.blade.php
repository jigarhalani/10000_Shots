@extends('adminpages.layouts.dashboard')
@section('page_heading','Scoreboard')
@section('section')
    @if(!empty($data))
        <div class="row">
            <div class="col-sm-12">

                @section ('htable_panel_title','Score')
                @section ('htable_panel_body')
                    @if(count($data)>0)
                    <div><label>Score :</label> <label>{{ count($data)*100 }}</label>
                    <div class="pull-right">
                        <a href="{{ url('admin/users/detail').'/'.$data[0]->candidate_id }}" title="View">
                        <button class="table-btn btn btn-info btn-xs"> <i class="fa fa-eye" aria-hidden="true"></i> {{ $data[0]->candidate->first_name }}</button></a>
                    </div>
                    </div>
                    @endif
                    <table id="scoreboard" class="wrapper display table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Shot Name</th>
                            <th class="no-sort">Video</th>
                            <th class="no-sort">Date</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $value)
                            <tr>
                                <td>@if(!empty($value->shot->shot_name)){{ $value->shot->shot_name }}@endif</td>
                                <td>
                                    <?php
                                    preg_match(
                                            '/[\\?\\&]v=([^\\?\\&]+)/',
                                            $value->video_link,
                                            $matches
                                    );
                                    ?>
                                    <iframe width="250" height="200" src="{{  'https://www.youtube.com/embed/'.$matches[1] }}" frameborder="0" allowfullscreen></iframe></td>
                                </td>
                                <td>{{ $value->created_at }}</td>
                            </tr>

                        @endforeach
                        </tbody>
                    </table>
                    {{--{{ $data->links() }}--}}
                @endsection
                @include('widgets.panel', array('header'=>true, 'as'=>'htable'))
            </div>
        </div>
    @endif
@stop
