@extends('adminpages.layouts.dashboard')
@section('page_heading','User')

@section('section')
        <div class="row">
            {{--@include('admin.sidebar')--}}

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><strong>User</strong>: {{ $data[0]->first_name.' '  }}{{ $data[0]->last_name }}</div>
                    <div class="panel-body">
                        <a href="{{ url('admin/users').'/'.$data[0]->candidate_type }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        {{--<img src="{{ asset('images/admin/').'/'.$data[0]->image }}" class="pull-right" style="width:100px;height:100px;">--}}
                        <br/><br/><br/><br/><br/><br/><br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>

                                <tr>   <th>First Name</th><td>{{ $data[0]->first_name }}</td> </tr>
                                <tr>   <th>Last Name</th><td>{{ $data[0]->last_name }}</td> </tr>
                                <tr>   <th>Email</th><td>{{ $data[0]->email_id }}</td> </tr>
                                <tr>   <th>Phone</th><td>{{ $data[0]->phone }}</td> </tr>
                                @if(!empty($data[0]->candidateInfo))
                                <tr><th>Date Of Birth</th><td>{{ $data[0]->candidateInfo->dob }}</td></tr>
                                <tr><th>Address</th><td>{{ $data[0]->candidateInfo->address_line1.','.$data[0]->candidateInfo->address_line2 }}</td></tr>
                                <tr><th>Country</th><td>{{ $data[0]->candidateInfo->country }}</td></tr>
                                <tr><th>State</th><td>{{ $data[0]->candidateInfo->state }}</td></tr>
                                <tr><th>City</th><td>{{ $data[0]->candidateInfo->city }}</td></tr>
                                @endif
                                <tr>   <th>Role</th><td>
                                        <?php if($data[0]->candidate_type==1){
                                            echo 'Coach';
                                        }elseif($data[0]->candidate_type==2){
                                            echo 'Player';
                                        }else{
                                            echo 'Parent';
                                        } ?>
                                    </td> </tr>
                                <tr>   <th>Status</th><td>
                                        @if($data[0]->is_active==1)
                                            <span class="label label-success">Active</span>
                                        @else
                                            <span class="label label-danger">Not Active</span>
                                        @endif
                                    </td> </tr>
                                @if(!empty($data[0]->team))
                                <tr><th>Team</th><td>{{ $data[0]->team->team_name }}</td></tr>
                                <tr><th>Team Description</th><td>{{ $data[0]->team->description }}</td></tr>
                                @endif
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
@endsection
