@extends('adminpages.layouts.dashboard')
@section('page_heading','Tricks & Tips')
@section('section')
    @include('adminpages.includes.notification')
    <div class="row">

        <div class="col-lg-12">

            <div class="panel panel-default">

                <div class="panel-heading">

                    Add Tricks & Tips

                </div>

                <div class="panel-body">

                    <div class="row">

                        <div class="col-lg-12">



                            <form role="form" id="tricks_form" method="POST" action="{{ url('admin/trick/save') }}">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label>Enter Trick Title</label>
                                    <input class="form-control" placeholder="Enter Trick Title" id="trick_title" name="trick_title" required minlength="2">
                                </div>
                                <div class="form-group">
                                    <label>Select Shots</label>
                                    <select id="shot_id" name="shot_id" class="form-control" required>
                                        @foreach($shots as $shot)
                                            <option value="{{$shot->shot_id}}">{{ $shot->shot_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Enter Trick Description</label>
                                    <textarea class="form-control" placeholder="Enter Trick Description" id="trick_description" name="trick_description" required></textarea>
                                    @ckeditor('trick_description')
                                </div>

                                <button type="submit" class="btn btn-default">Add</button>
                                <button type="reset" class="btn btn-default">Reset</button>

                            </form>

                        </div>

                    </div>

                    <!-- /.row (nested) -->

                </div>

                <!-- /.panel-body -->

            </div>

            <!-- /.panel -->

        </div>

        <!-- /.col-lg-12 -->

    </div>
@stop
