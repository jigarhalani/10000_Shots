@extends('adminpages.layouts.dashboard')
@section('page_heading','Tricks & Tips')
@section('section')
    @include('adminpages.includes.notification')
    <div class="row">

        <div class="col-lg-12">

            <div class="panel panel-default">

                <div class="panel-heading">

                    View Tricks & Tips

                </div>

                <div class="panel-body">

                    <div class="table-responsive">

                        {{ $tricks->links() }}
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">

                            <thead>

                            <tr>
                                <th>Trick Title</th>
                                <th>Trick Description</th>
                                <th>Shots Name</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>

                            </thead>

                            <tbody>

                            @if(count($tricks)>0)
                            @foreach($tricks as $trick)
                            <tr class="odd gradeX">
                                <td>{{  $trick->trick_title }}</td>
                                <td>{!!    $trick->trick_description !!}</td>
                                <td>{{  $trick->shot->shot_name }}</td>
                                <td class="center"><a href="{{ url('admin/trick/edit/'.$trick->trick_id) }}">Edit</a></td>
                                <td class="center"><a
                                            href="{{ url('admin/trick/delete/'.$trick->trick_id) }}" onclick="return confirm('Are you sure you want to delete?')">Delete</a>
                                </td>
                            </tr>

                            @endforeach
                            @else
                                <tr class="warning">
                                    <td colspan="5"><i class="fa fa-warning"></i> No result</td>
                                </tr>
                            @endif

                            </tbody>

                        </table>
                        {{ $tricks->links() }}
                    </div>

                    <!-- /.table-responsive -->

                </div>

                <!-- /.panel-body -->

            </div>

            <!-- /.panel -->

        </div>

        <!-- /.col-lg-12 -->

    </div>
@stop
