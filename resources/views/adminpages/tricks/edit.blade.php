@extends('adminpages.layouts.dashboard')
@section('page_heading','Tricks & Tips')
@section('section')
    @include('adminpages.includes.notification')
    <div class="row">

        <div class="col-lg-12">

            <div class="panel panel-default">

                <div class="panel-heading">

                    Edit Tricks & Tips

                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form role="form" id="tricks_form" method="POST" action="{{ url('admin/trick/update/'.$tricks->trick_id) }}">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label>Enter Trick Title</label>
                                    <input class="form-control" placeholder="Enter Trick Title" id="trick_title" name="trick_title" required minlength="2" value="{{ $tricks->trick_title }}">
                                </div>

                                <div class="form-group">
                                    <label>Select Shots</label>
                                    <select id="shot_id" name="shot_id" class="form-control" required>
                                        @foreach($shots as $shot)
                                            @if($shot->shot_id==$tricks->shot_id)
                                                <option value="{{ $shot->shot_id }}" selected>{{ $shot->shot_name }}</option>
                                            @else
                                                <option value="{{ $shot->shot_id }}">{{ $shot->shot_name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Enter Trick Description</label>
                                    <textarea class="form-control" placeholder="Enter Trick Description" id="trick_description" name="trick_description" required>{{ $tricks->trick_description}}</textarea>
                                    @ckeditor('trick_description')
                                </div>

                                <button type="submit" class="btn btn-default">Update</button>
                                <button type="reset" class="btn btn-default">Reset</button>

                            </form>

                        </div>

                    </div>

                    <!-- /.row (nested) -->

                </div>

                <!-- /.panel-body -->

            </div>

            <!-- /.panel -->

        </div>

        <!-- /.col-lg-12 -->

    </div>
@stop
