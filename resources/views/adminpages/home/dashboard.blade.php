@extends('adminpages.layouts.dashboard')
@section('page_heading','Dashboard')
@section('section')
    @include('adminpages.includes.notification')
    <script type="text/javascript" src="{{ asset("assets/scripts/amcharts.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/scripts/serial.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/scripts/none.js") }}"></script>
    <div class="row">

        <div class="col-lg-12">

            <div class="panel panel-default">

                <div class="panel-heading">

                    Dashboard

                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa  fa-user fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge">{{ $total['player_total'] }}</div>
                                            <div>Total Players!</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="{{ url('admin/users/2') }}">
                                    <div class="panel-footer">
                                        <span class="pull-left">Go To</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-green">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-user fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge">{{ $total['coach_total'] }}</div>
                                            <div>Total Coach!</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="{{ url('admin/users/1') }}">
                                    <div class="panel-footer">
                                        <span class="pull-left">Go To</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-yellow">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-user fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge">{{ $total['parents_total'] }}</div>
                                            <div>Total Parents</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="{{ url('admin/users/3') }}">
                                    <div class="panel-footer">
                                        <span class="pull-left">GO To</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-red">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa  fa-table fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge">{{ $total['shots_total'] }}</div>
                                            <div>Total Shots</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="{{ url('admin/shots/view') }}">
                                    <div class="panel-footer">
                                        <span class="pull-left">Go To</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                    <!-- CHart code -->
                    <div class="row">
                        <script>
                            var chart = AmCharts.makeChart("chartdiv", {
                                "type": "serial",
                                "theme": "none",
                                "dataProvider": [{

                                    "country": "Player",
                                    "visits": {{ $total['player_total']}},
                                    "color": "#F8FF01"
                                }, {
                                    "country": "Coach",
                                    "visits": {{ $total['coach_total']}},
                                    "color": "#04D215"
                                },  {
                                    "country": "Parents",
                                    "visits": {{ $total['parents_total']}},
                                    "color": "#FF0F00"

                                }, {
                                    "country": "Shots",
                                    "visits": {{ $total['shots_total']}},
                                    "color": "#CD0D74"
                                }],
                                "valueAxes": [{
                                    "axisAlpha": 0,
                                    "position": "left",
                                    "title": "Total Counts"
                                }],
                                "startDuration": 1,
                                "graphs": [{
                                    "balloonText": "<b>[[category]]: [[value]]</b>",
                                    "colorField": "color",
                                    "fillAlphas": 0.9,
                                    "lineAlpha": 0.2,
                                    "type": "column",
                                    "valueField": "visits"
                                }],
                                "chartCursor": {
                                    "categoryBalloonEnabled": false,
                                    "cursorAlpha": 0,
                                    "zoomable": false
                                },
                                "categoryField": "country",
                                "categoryAxis": {
                                    "gridPosition": "start",
                                    "labelRotation": 45
                                },
                                "exportConfig":{
                                    "menuTop":"20px",
                                    "menuRight":"20px",
                                    "menuItems": [{
                                        "icon": '/lib/3/images/export.png',
                                        "format": 'png'
                                    }]
                                }
                            });


                        </script>
                        <div id="chartdiv">
                        </div>
                    </div>
                    <!-- endCHart code -->

                </div>

                <!-- /.panel-body -->

            </div>

            <!-- /.panel -->

        </div>

        <!-- /.col-lg-12 -->

    </div>
@stop
