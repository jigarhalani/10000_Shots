@extends ('adminpages.layouts.plane')

<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <br /><br /><br />
            @section ('login_panel_title','Please Sign In')
            @section ('login_panel_body')
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form role="form" method="POST" action="{{ url('admin/login') }}">

                    <fieldset>
                        <div class="form-group">
                            <input id="username" class="form-control" name="username" value="{{ old('username') }}" required autofocus>
                        </div>
                        <div class="form-group">
                            <input id="password" type="password" class="form-control" required name="password">
                        </div>
                        <!-- Change this to a button or input when using this as a form -->
                        <button class="btn btn-lg btn-primary btn-block">Login</button>
                        <a href="{{ url('admin/password/reset') }}">Forgot Your Password?</a>
                    </fieldset>
                </form>

            @endsection
            @include('widgets.panel', array('as'=>'login', 'header'=>true))
        </div>
    </div>
</div>
