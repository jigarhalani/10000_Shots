@extends('frontpages.layouts.plane')
@section('body')
 <div id="wrapper">
     <script type="text/javascript">
         var base_url = {!! "'".URL::to('/')."/'" !!};
     </script>
        <div id="page-wrapper">
            @yield('section')
        </div>
    </div>

@stop

