<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
	<meta charset="utf-8"/>
	<title>1000Shots-App</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>
	<link rel="stylesheet" href="{{ asset("assets/stylesheets/front/bootstrap.min.css") }}" />
	<link rel="stylesheet" href="{{ asset("assets/stylesheets/front/font-awesome.min.css") }}">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" />
	<link rel="stylesheet" type="text/css" href="{{ asset("assets/stylesheets/front/footer_navbar.css") }}">
	<link rel="stylesheet" href="{{ asset("assets/stylesheets/front/custom_css.css") }}" />


</head>
<?php $clas = (!empty($class) && $class=='no') ? '' : 'bacgground_color'; ?>
<body class="container {{ $clas }}">
	@yield('body')
	<script src="//code.jquery.com/jquery-1.12.4.js"></script>
	<script src="{{ asset("assets/scripts/front/bootstrap.js") }}" type="text/javascript"></script>
	<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js" type="text/javascript"> </script>
	<script src="{{ asset("assets/scripts/front/footer_navbar.js") }}" type="text/javascript"></script>
	<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
	<script src="{{ asset("assets/scripts/front/validation.js") }}"></script>

</body>
</html>
<!DOCTYPE html>
<html lang="en">
