<?php $bgclas = (!empty($bg) && $bg=='yes') ? 'bacgground_color' : ''; ?>
<hr>
<nav class="navbar  navbar-bottom {{ $bgclas }}" id="mobile_nav">
    <ul class="nav navbar-nav  mobile-bar">
        <li>
            <a href="{{ url('front/shots') }}">
                <span class="menu-icon"><img src="{{ asset('assets/images/shots.png') }}" class="img-responsive " ></span>
                <h4>Shots</h4>
            </a>
        </li>
        <li>
            <a href="#">
                <span class="menu-icon "><img src="{{ asset('assets/images/profile.png') }}" class="img-responsive " ></span>
                <span class="hidden-xs"><h4> Profile</h4></span>
                <span class="visible-xs"><h4> Profile</h4></span>
            </a>
        </li>
        <li class="hidden-xs">
            <a href="#">
                <span class="menu-icon fa fa-picture-o"></span>
                Photos
            </a>
        </li>
        <li>
            <a href="#">
                <span class="menu-icon "><img src="{{ asset('assets/images/challange.png') }}" class="img-responsive " ></span>
                <h4>Challange</h4>
            </a>
        </li>
        <li class="visible-xs">
            <a href="#">
                <span class="menu-icon "><img src="{{ asset('assets/images/more.png') }}" class="img-responsive " ></span>
                <h4>More</h4>
            </a>
        </li>
    </ul>

</nav>
<nav class="navbar navbar-bottom {{ $bgclas }}" id="dektop_nav" >

    <div class="navbar-collapse collapse">


        <div class=" col-md-12  pd-t-10" >
            <!--  <ul class="nav navbar-nav  navbar-centerr"> -->
            <ul class="nav navbar-nav navbar-centerr desktop_bar">
                <li>
                    <a href="{{ url('front/shots') }}">
                        <span class="menu-icon"><img src="{{ asset('assets/images/shots.png') }}" class="img-responsive " ></span>
                        <h4>Shots</h4>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="menu-icon "><img src="{{ asset('assets/images/profile.png') }}" class="img-responsive " ></span>
                        <span class="hidden-xs"><h4> Profile</h4></span>

                    </a>
                </li>
                <li class="hidden-xs">
                    <a href="#">
                        <span class="menu-icon "><img src="{{ asset('assets/images/challange.png') }}" class="img-responsive " ></span>
                        <h4>Challange</h4>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="menu-icon"><img src="{{ asset('assets/images/more.png') }}" class="img-responsive " ></span>
                        <h4>More</h4>
                    </a>
                </li>
                <li class="visible-xs">
                    <a href="#navbar-more-show">
                        <span class="menu-icon fa fa-bars"></span>
                        More
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>