<?php $class='no'; ?>
@extends('frontpages.layouts.dashboard')
@section('page_heading','FrontEnd')

@section('section')

    @if(\Illuminate\Support\Facades\Session::has('error'))
        <div class="alert {{ \Illuminate\Support\Facades\Session::get('error_class') }}">
            <strong>{{ \Illuminate\Support\Facades\Session::get('error') }}</strong>
        </div>
    @endif
    @if(\Illuminate\Support\Facades\Session::has('success'))
        <div class="alert alert-success">
            <strong>{{ \Illuminate\Support\Facades\Session::get('success') }}</strong>
        </div>
    @endif
    <div class="col-md-12   thank_you" align="center" >
        <h1 class="site_color_org "><b>Login</b> </h1>
    </div>

    {{--<div class="col-md-12 thank_you" align="center">
        <h3></h3>
    </div>--}}
    <form id="login_form" name="login_form" action="{{ url('login') }}" method="post">
        <div class="col-md-12 thank_you_email" align="center">
            <input class="img-responsive" name="email" type="email"  placeholder="Enter Email" size="35" style="padding:5px;">
        </div>
        <div class="col-md-12 thank_you_email" align="center">
            <input class="img-responsive" type="password" name="password" id="password"  placeholder="Enter Password" size="35" style="padding:5px;">
        </div>
        <div class="col-md-12 thank_you_email" align="center">
            <input class="start_button" type="submit"  name="Start" value="Login">
        </div>
    </form>
@endsection
