
@if(!empty($error))
    <div class="alert {{ $error_class }}">
        <strong>{{ $error }}</strong>
    </div>
@endif
