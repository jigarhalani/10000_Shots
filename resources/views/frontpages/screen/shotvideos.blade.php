@extends('frontpages.layouts.dashboard')
@section('page_heading','FrontEnd')

@section('section')
    <div class="col-md-12   site_color_org" align="center"><h2>{{ $shot->shot_name }} Videos</h2></div>
    <div class="ahsan_list">
        @if(count($videos)>0)
            @foreach($videos as $video)
                <hr>
                <li  align="center" >
                    <?php
                    preg_match(
                            '/[\\?\\&]v=([^\\?\\&]+)/',
                            $video->video_link,
                            $matches
                    );
                    ?>
                    <iframe width="560" height="315" src="{{  'https://www.youtube.com/embed/'.$matches[1] }}" frameborder="0" allowfullscreen></iframe>
                </li>
            @endforeach
        @else
            <h1>No Videos Found</h1>
        @endif
    </div>
    @include('frontpages.layouts.navbar')
@endsection
