@extends('frontpages.layouts.dashboard')
@section('page_heading','FrontEnd')

@section('section')
    <div class="col-md-12   site_color_org" align="center"><h2>PICK YOUR SHOT!</h2></div>
    <a href="{{ url('front/videos') }}"><h3 class="col-md-12 text_color_white underline" align="center">How to shoot videos</h3></a>
    <br>
    <div class="ahsan_list">
        @if(count($shots)>0)
        @foreach($shots as $shot)
            <li><a href="{{ url('front/shots/'.$shot->shot_id) }}" class="unactive"><b>{{ $shot->shot_name }}</b></a> </li>
        @endforeach
            @else
            <h1>No Shots Found</h1>
            @endif
    </div>
    @include('frontpages.layouts.navbar')
@endsection
