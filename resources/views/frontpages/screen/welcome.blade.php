<?php $class='no'; ?>
@extends('frontpages.layouts.dashboard')
@section('page_heading','FrontEnd')

@section('section')
    @if (\Illuminate\Support\Facades\Session::has('error'))
    <div class="alert alert-danger"> {{ \Illuminate\Support\Facades\Session::get('error') }}</div>
    @endif
    @if(\Illuminate\Support\Facades\Session::has('success'))
        <div class="alert alert-success">
            <strong>{{ \Illuminate\Support\Facades\Session::get('success') }}</strong>
        </div>
    @endif
    <div class="col-md-12   thank_you" align="center" >
        <h1 class="site_color_org "><b>WELCOME</b> </h1>
    </div>

    <div class="col-md-12 thank_you" align="center">
        <h3 class="login">Register </h3>
    </div>
    <form action="{{ url('checkUser') }}" method="post">
        <div class="col-md-12 " align="center">
            <input class="start_button coach_button" type="submit"  name="coach" value="Coach">
        </div>
        <div class="col-md-12 " align="center">
            <input class="player_button" type="submit"  name="player" value="Player">
        </div>
    </form>
@endsection
