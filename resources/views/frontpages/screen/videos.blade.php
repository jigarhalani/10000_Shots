<?php $class='no'; ?>
@extends('frontpages.layouts.dashboard')
@section('page_heading','FrontEnd')

@section('section')
    <div class="col-md-12" align="center" >
        <h1 class="site_color_org "><b>HOW TO SHOOT</b> </h1>
    </div>
    @if(count($videos)>0)
            @foreach($videos as $video)
                <hr>
                <div style="display:inline-block;" >
                    <?php
                    preg_match(
                            '/[\\?\\&]v=([^\\?\\&]+)/',
                            $video->video_link,
                            $matches
                    );
                    ?>
                    <iframe width="560" height="315" src="{{  'https://www.youtube.com/embed/'.$matches[1] }}" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="vedio_text" >
                    <h4 class="text_youtube">Title: {{ $video->video_title }}</h4>
                    <h2 class="">Shot Name: {{ $video->shot->shot_name }}</h2>
                    <br><br>
                    <h4 class="text_youtube">YouTube</h4>
                </div>
            @endforeach
        @else
            <h1>No Videos Found</h1>
        @endif
    <?php $bg='yes'; ?>
    @include('frontpages.layouts.navbar')
@endsection