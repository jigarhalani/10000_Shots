@extends('frontpages.layouts.dashboard')
@section('page_heading','FrontEnd')

@section('section')
    <div class="col-md-12   site_color_org" align="center"><h2>{{ $shots->shot_name }}</h2></div>
    <a href="{{ url('front/videos') }}"><h3 class="col-md-12 text_color_white underline" align="center">How to shoot videos</h3></a>
    <br>
    <div class="col-md-12 " align="center">
        <h1 class="text_color_white digital_font "> <sup class="site_color_org">+</sup>100</h1>
    </div>
    <br>
    <div > <p class="text_color_white clo-md-12">{{ $shots->shot_description }}</p></div>
    <br>
    <div class="ahsan_list">
        @if(count($videos)>0)
                <hr>
                <li  align="center" >
                    <?php
                    preg_match(
                            '/[\\?\\&]v=([^\\?\\&]+)/',
                            $videos[0]->video_link,
                            $matches
                    );
                    ?>
                    <iframe width="560" height="315" src="{{  'https://www.youtube.com/embed/'.$matches[1] }}" frameborder="0" allowfullscreen></iframe>
                </li>
         @endif
    </div>
    @if(count($videos)>1)
        <div class="ahsan_list">
            <li><a href="video/{{ $shots->shot_id }}" class="unactive"><b>More Videos</b></a></li>
        </div>
    @endif
    @if(count($videos)==0)
        <h1>No Videos Found</h1>
    @endif
    @include('frontpages.layouts.navbar')
@endsection
