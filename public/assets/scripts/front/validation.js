$("#shots_form").validate();
$("#request_registration_form").validate();
$("#tricks_form").validate();
$("#video_form").validate({
    rules: {
        video_link: {
            required: true,
            url: true
        }
    }
});
$("#change_password").validate({
    rules: {
        password: {
            required: true,
        },
        password_confirmation: {
            equalTo: "#password"
        }
    }
});
$("#register_form").validate({
    rules: {
        password: {
            required: true,
        },
        r_password: {
            equalTo: "#password",
            required:true
        },
        email_id:{
            email:true,
            required:true
        },
        parents_email:{
            email:true,
            required:true
        }
    },
    errorPlacement: function(error, element) {
        error.appendTo( element.parent("div") );
    }

});
$("#login_form").validate({
    rules: {
        password: {
            required: true,
        },
        email:{
            email:true,
            required:true
        },
    },
    errorPlacement: function(error, element) {
        error.appendTo( element.parent("div") );
    }

});
$("#add_admin").validate({
    rules: {
        first_name: {
            required: true,
        },last_name: {
            required: true,
        },email: {
            required: true,
            email:true,
            remote: {
                url: base_url+'admin/checkmail',
                type: "post",
            }
        },username: {
            required: true,
            remote: {
                url: base_url+'admin/checkusername',
                type: "post",
            }
        },phone: {
            required: true,
            number:true
        },
    },
    messages: {
        email: {
            required: "Please enter your email address.",
            email: "Please enter a valid email address.",
            remote: "Email already in use!"
        },
        username: {
            required: "Please enter username.",
            remote: "Username already in use!"
        }
    },
});
$('.alert-danger').delay(3000).fadeOut();
$('.alert-success').delay(3000).fadeOut();

$(document).on('keyup',".search",function () {
        var searchTerm = $(".search").val();
        var listItem = $('.results tbody').children('tr');
        var searchSplit = searchTerm.replace(/ /g, "'):containsi('")

        $.extend($.expr[':'], {'containsi': function(elem, i, match, array){
            return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
        }
        });

        $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
            $(this).attr('visible','false');
        });

        $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
            $(this).attr('visible','true');
        });

        var jobCount = $('.results tbody tr[visible="true"]').length;
        $('.counter').text(jobCount + ' item');

        if(jobCount == '0') {$('.no-result').show();}
        else {$('.no-result').hide();}
});

$(document).on('change','#user_type_filter',function () {
    $user_type=$(this).val();
    $user_status=$('#user_status_filter').val();
    window.location=base_url+'admin/email/'+$user_type+'/'+$user_status;
});

$(document).on('change','#user_status_filter',function () {
    $user_type=$('#user_type_filter').val();
    $user_status=$(this).val();
    window.location=base_url+'admin/email/'+$user_type+'/'+$user_status;
});